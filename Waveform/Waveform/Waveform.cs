namespace Laserbrain.Waveform
{
	using System;
	using System.Collections.Generic;

	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Audio;
	using Microsoft.Xna.Framework.Graphics;
	using Microsoft.Xna.Framework.Input;
	using Microsoft.Xna.Framework.Input.Touch;

	public class Waveform : Game
	{
		// The size of the screen.
		private const int LandscapeWidth = 800;
		private const int LandscapeHeight = 480;

		// The number of milliseconds per time slice.
		private const int SliceMilliseconds = 100;

		// The number of pixels to change color.
		private const int ChangeColorSteps = 50;
		private readonly Color[] sampleColors = new[]
			{
				new Color(0.9f, 0.2f, 0.2f, 0.07f),
				new Color(0.8f, 0.7f, 0.2f, 0.07f),
				new Color(0.1f, 0.9f, 0.2f, 0.07f),
				new Color(0.1f, 0.6f, 0.7f, 0.07f),
				new Color(0.2f, 0.3f, 1.0f, 0.07f),
				new Color(0.8f, 0.2f, 0.9f, 0.07f),
			};

		// The microphone and the sample data.
		private readonly Microphone microphone;
		private readonly byte[] microphoneData;

		// The time it takes for a sample to pass over the screen.
		private readonly TimeSpan screenMilliseconds = TimeSpan.FromSeconds(5);

		// The sprite batch and a white single-pixel texture for drawing.
		private SpriteBatch spriteBatch;
		private Texture2D whitePixelTexture;

		// The size in pixels of one time slice.
		private int imageSliceWidth;
		private int imageSliceHeight;

		// The first, last and current image slices.
		private LinkedListNode<RenderTarget2D> firstImageSlice;
		private LinkedListNode<RenderTarget2D> lastImageSlice;
		private LinkedListNode<RenderTarget2D> currentImageSlice;

		// The current speed of the samples.
		private float pixelsPerSeconds;

		// The time in seconds when the current microphone data appeared.
		private float microphoneDataAppearedAtSeconds;

		// A flag telling whether new data has been read.
		private bool hasNewMicrophoneData;

		// The number of samples squeezed into the width of one pixel.
		private int samplesPerPixel;

		// Was the user touching the screen during the last Update-method?
		private bool wasTouching;

		// The color of the samples.
		private Color sampleColor;
		private int changeColorStep;
		private float colorR;
		private float colorG;
		private float colorB;
		private float colorDr;
		private float colorDg;
		private float colorDb;
		private int nextColorIndex;

		public Waveform()
		{
			// Set the screen mode.
			new GraphicsDeviceManager(this)
			{
				PreferredBackBufferWidth = LandscapeWidth,
				PreferredBackBufferHeight = LandscapeHeight,
				IsFullScreen = true,
				SupportedOrientations =
					DisplayOrientation.Portrait |
					DisplayOrientation.LandscapeLeft |
					DisplayOrientation.LandscapeRight
			};

			// Standard setup.
			Content.RootDirectory = "Content";
			TargetElapsedTime = TimeSpan.FromTicks(333333);
			InactiveSleepTime = TimeSpan.FromSeconds(1);

			// Refer to the default microphone and hook the BufferReady-event.
			microphone = Microphone.Default;
			microphone.BufferReady += MicrophoneBufferReady;

			// Set the buffer duration to the length of one slice.
			microphone.BufferDuration = TimeSpan.FromMilliseconds(SliceMilliseconds);

			// Calculate the size in bytes of the sound buffer and create the byte array.
			var microphoneDataLength = microphone.GetSampleSizeInBytes(microphone.BufferDuration);
			microphoneData = new byte[microphoneDataLength];

			// Start listening.
			microphone.Start();

			sampleColor = sampleColors[0];
		}

		protected override void LoadContent()
		{
			// Create a SpriteBatch for drawing.
			spriteBatch = new SpriteBatch(GraphicsDevice);

			// Create a 1x1 pixel big texture containing a white pixel.
			whitePixelTexture = new Texture2D(GraphicsDevice, 1, 1);
			var white = new[] { Color.White };
			whitePixelTexture.SetData(white);

			// Create the image slices.
			CreateSliceImages();
		}

		protected override void UnloadContent()
		{
			// Dispose the SpriteBatch.
			spriteBatch.Dispose();

			// Dispose the white pixel.
			whitePixelTexture.Dispose();

			// Dispose all the image slices.
			var slice = firstImageSlice;
			while (slice != null)
			{
				slice.Value.Dispose();
				slice = slice.Next;
			}
		}

		protected override void Update(GameTime gameTime)
		{
			// Exit the app if the user presses the back-button.
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
			{
				Exit();
			}

			var isTouching = TouchPanel.GetState().Count > 0;
			if (isTouching && !wasTouching && (changeColorStep == 0))
			{
				InitColorChange();
			}
			wasTouching = isTouching;

			// If new data has been captured, a new slice should be drawn.
			if (hasNewMicrophoneData)
			{
				// Reset the flag.
				hasNewMicrophoneData = false;

				// Express the current point in time as "seconds passed since start of app".
				var currentSeconds = (float)gameTime.TotalGameTime.TotalSeconds;

				// Remember the current time as "when the mic data appeared".
				microphoneDataAppearedAtSeconds = currentSeconds;

				// Render the new samples on the current image slice.
				RenderSamples(currentImageSlice.Value);

				// Select the next image slice as the new "current".
				currentImageSlice = currentImageSlice.Next ?? firstImageSlice;
			}

			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			// Clear the device. (Actually unnecessary since the whole screen will be painted below.)
			GraphicsDevice.Clear(Color.Black);

			// Calculate the "screen width-scale", to allow the app to be drawn both in
			// Landscape and Portrait mode.
			// In Landscape mode, the screenWidthScale will be 1.0 (i.e. original scale)
			// In Portrait mode, the screen must be squeezed.
			var screenWidthScale = (float)GraphicsDevice.Viewport.Width / LandscapeWidth;

			// Calculate the scaled width of one slice.
			var scaledWidth = (int)Math.Ceiling(imageSliceWidth * screenWidthScale);

			// Express the current point in time as "seconds passed since start of app".
			var currentSeconds = (float)gameTime.TotalGameTime.TotalSeconds;

			// Calculate how many seconds that has passed since the current microphone data was captured.
			var secondsPassed = currentSeconds - microphoneDataAppearedAtSeconds;

			// For a smooth move of the pixels, calculate the offset of the current microphone data
			// (where the offset is zero at the time of the new data arrived, and then growing up
			// one full width of a slice.
			var drawOffsetX = secondsPassed * pixelsPerSeconds;

			// Since it is not certain that the next microphone data will come before the current
			// slice has moved its full distance, the offset needs to be truncated so it doesn't
			// move too far.
			if (drawOffsetX > scaledWidth)
			{
				drawOffsetX = scaledWidth;
			}

			try
			{
				// Start draw the slices
				spriteBatch.Begin();

				// Start with one slice before the current one, wrap if necessary.
				var imageSlice = currentImageSlice.Previous ?? lastImageSlice;

				// Prepare the rectangle to draw within, starting with the newest
				// slice far to the right of the screen (a bit outside, even).
				var destinationRectangle = new Rectangle(
					(int)(GraphicsDevice.Viewport.Width + scaledWidth - drawOffsetX),
					0,
					scaledWidth,
					GraphicsDevice.Viewport.Height);

				// Draw the slices in the linked list one by one from the right
				// to the left (from the newest sample to the oldest)
				// and move the destinationRectangle one slice-width at a time
				// until the full screen is covered.
				while (destinationRectangle.X > -scaledWidth)
				{
					// Draw the current image slice.
					spriteBatch.Draw(imageSlice.Value, destinationRectangle, Color.White);

					// Move the destinationRectangle one step to the left.
					destinationRectangle.X -= scaledWidth;

					// Select the previous image slice to draw next time, wrap if necessary.
					imageSlice = imageSlice.Previous ?? lastImageSlice;
				}
			}
			finally
			{
				// Drawing done.
				spriteBatch.End();
			}

			base.Draw(gameTime);
		}

		private void CreateSliceImages()
		{
			// Calculate how many slices that fits the screen (rounding upwards).
			var imageSlicesOnScreenCount = (int)Math.Ceiling(screenMilliseconds.TotalMilliseconds / SliceMilliseconds);

			// Calculate the width of each slice.
			imageSliceWidth = (int)Math.Ceiling((float)LandscapeWidth / imageSlicesOnScreenCount);

			// Set the height of each slice to the largest screen size
			// (this way the full height is utilized in Portrait mode without stretching)
			imageSliceHeight = LandscapeWidth;

			// Create a linked list with the required number of slices, plus two
			// so that there's room for scrolling off-screen a bit.
			var imageSlices = new LinkedList<RenderTarget2D>();
			for (var i = 0; i < imageSlicesOnScreenCount + 2; i++)
			{
				var imageSlice = new RenderTarget2D(GraphicsDevice, imageSliceWidth, imageSliceHeight);
				imageSlices.AddLast(imageSlice);
			}

			// Reference the first, last and current slice.
			firstImageSlice = imageSlices.First;
			lastImageSlice = imageSlices.Last;
			currentImageSlice = imageSlices.Last;

			// Calculate the speed of the pixels for an image slice.
			pixelsPerSeconds = imageSliceWidth / (SliceMilliseconds / 1000f);

			// Since the byte-array buffer really holds 16-bit samples, the actual
			// number of samples in one buffer is the number of bytes divided by two.
			var sampleCount = microphoneData.Length / 2;

			// Calculate how many samples that should be squeezed in per pixel (width).
			samplesPerPixel = (int)Math.Ceiling((float)sampleCount / imageSliceWidth);

			// Iterate through all the image slices and render with the empty microphone buffer.
			var slice = firstImageSlice;
			while (slice != null)
			{
				RenderSamples(slice.Value);
				slice = slice.Next;
			}
		}

		private void MicrophoneBufferReady(object sender, EventArgs e)
		{
			// New microphone data can now be fetched from its buffer.

			// Copy the samples from the microphone buffer to our buffer.
			microphone.GetData(microphoneData);

			// Raise the flag that new data has come.
			hasNewMicrophoneData = true;
		}

		private void RenderSamples(RenderTarget2D target)
		{
			try
			{
				// Redirect the drawing to the given target.
				GraphicsDevice.SetRenderTarget(target);

				// Clear the target slice.
				GraphicsDevice.Clear(Color.Black);

				// Begin to draw. Use Additive for an interesting effect.
				spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive);

				// The X-variable points out to which column of pixels to
				// draw on.
				var x = 0;

				// Since the byte-array buffer really holds 16-bit samples, the actual
				// number of samples in one buffer is the number of bytes divided by two.
				var sampleCount = microphoneData.Length / 2;

				// The index of the current sample in the microphone buffer.
				var sampleIndex = 0;

				// The vertical mid point of the image (the Y-position
				// of a zero-sample and the height of the loudest sample).
				var halfHeight = imageSliceHeight / 2;

				// The maximum number of a 16-bit signed integer.
				// Dividing a signed 16-bit integer (the range -32768..32767)
				// by this value will give a value in the range of -1 (inclusive) to 1 (exclusive).
				const float SampleFactor = 32768f;

				UpdateColorChange();

				// Iterate through the samples and render them on the image.
				for (var i = 0; i < sampleCount; i++)
				{
					// Increment the X-coordinate each time 'samplesPerPixel' pixels
					// has been drawn.
					if ((i > 0) && ((i % samplesPerPixel) == 0))
					{
						UpdateColorChange();
						x++;
					}

					// Convert the current sample (16-bit value) from the byte-array to a
					// floating point value in the range of -1 (inclusive) to 1 (exclusive).
					var sampleValue = BitConverter.ToInt16(microphoneData, sampleIndex) / SampleFactor;

					// Scale the sampleValue to its corresponding height in pixels.
					var sampleHeight = (int)Math.Abs(sampleValue * halfHeight);

					// The top of the column of pixels.
					// A positive sample should be drawn from the center and upwards,
					// and a negative sample from the center and downwards.
					// Since a rectangle is used to describe the "pixel column", the
					// top must be modified depending on the sign of the sample (positive/negative).
					var y = (sampleValue < 0)
						? halfHeight
						: halfHeight - sampleHeight;

					// Create the 1 pixel wide rectangle corresponding to the sample.
					var destinationRectangle = new Rectangle(x, y, 1, sampleHeight);

					// Draw using the white pixel (stretching it to fill the rectangle).
					spriteBatch.Draw(
						whitePixelTexture,
						destinationRectangle,
						sampleColor);

					// Step the two bytes-sample.
					sampleIndex += 2;
				}
			}
			finally
			{
				// Drawing done.
				spriteBatch.End();

				// Restore the normal rendering target (the screen).
				GraphicsDevice.SetRenderTarget(null);
			}
		}

		//private void InitColorChange()
		//{
		//    const float WantedTotalColorStrength = 1f;

		//    var r = (float)random.NextDouble();
		//    var g = (float)random.NextDouble();
		//    var b = (float)random.NextDouble();

		//    var sum = r + g + b;
		//    var scale = WantedTotalColorStrength / sum;
		//    r *= scale;
		//    g *= scale;
		//    b *= scale;

		//    colorDr = (r - colorR) / ChangeColorSteps;
		//    colorDg = (g - colorG) / ChangeColorSteps;
		//    colorDb = (b - colorB) / ChangeColorSteps;
		//    changeColorStep = ChangeColorSteps;
		//}

		private void InitColorChange()
		{
			nextColorIndex++;
			if (nextColorIndex == sampleColors.Length)
			{
				nextColorIndex = 0;
			}

			colorDr = ((sampleColors[nextColorIndex].R / 255f) - colorR) / ChangeColorSteps;
			colorDg = ((sampleColors[nextColorIndex].G / 255f) - colorG) / ChangeColorSteps;
			colorDb = ((sampleColors[nextColorIndex].B / 255f) - colorB) / ChangeColorSteps;
			changeColorStep = ChangeColorSteps;
		}

		private void UpdateColorChange()
		{
			if (changeColorStep == 0)
			{
				return;
			}

			changeColorStep--;
			colorR += colorDr;
			colorG += colorDg;
			colorB += colorDb;

			sampleColor = new Color(colorR, colorG, colorB, 0.07f);
		}
	}
}